# RealSense Position Tracking with ADS Stream

This project is a simple demo of using the RealSense camera by Intel to stream data to a TwinCAT 3 PLC.



The data being streamed is only the left and right hand data, but it could easily be adapted to stream more points.



![D415](Images/D415.jpg)

### Prerequisites

- Intel RealSense SDK 2.0+
- NuiTrack SDK
- TwinCAT 3.1



### How to Run

Install the above mentioned SDKs, and load 2 PLC structs into the TwinCAT 3.1 system; **RealSense_LeftHand**, **RealSense_RightHand**.



After launching the .Net application, click the dropdown to select your route and press connect button to start the stream.

![TrackerLive](Images/TrackerLive.png)



The data being streamed is similar to what is available in the NuiTrack Hand Tracking SDK Class:

| Datatype | Name/Description                                             |
| -------- | ------------------------------------------------------------ |
| float    | [x](https://download.3divi.com/Nuitrack/doc/structtdv_1_1nuitrack_1_1Hand.html#a96898064b84f9c90561a682eeb2d8466) |
|          | The normalized projective x coordinate of the hand (in range [0, 1]). |
|          |                                                              |
| float    | [y](https://download.3divi.com/Nuitrack/doc/structtdv_1_1nuitrack_1_1Hand.html#a58c2f9e877258be4f66f944ccba3cbc5) |
|          | The normalized projective y coordinate of the hand (in range [0, 1]). |
|          |                                                              |
| bool     | [click](https://download.3divi.com/Nuitrack/doc/structtdv_1_1nuitrack_1_1Hand.html#a50977842e84b2970dce67abd905269f4) |
|          | True if the hand makes a click, false otherwise.             |
|          |                                                              |
| int      | [pressure](https://download.3divi.com/Nuitrack/doc/structtdv_1_1nuitrack_1_1Hand.html#a49b6a85a7202a8f2e213ca9067ce77a3) |
|          | Rate of hand clenching.                                      |
|          |                                                              |
| float    | [xReal](https://download.3divi.com/Nuitrack/doc/structtdv_1_1nuitrack_1_1Hand.html#ae3044d983e831546961f9842a05c3c6f) |
|          | The x coordinate of the hand in the world system.            |
|          |                                                              |
| float    | [yReal](https://download.3divi.com/Nuitrack/doc/structtdv_1_1nuitrack_1_1Hand.html#a6da9363c66f8aa8e3e417d717817aeb9) |
|          | The y coordinate of the hand in the world system.            |
|          |                                                              |
| float    | [zReal](https://download.3divi.com/Nuitrack/doc/structtdv_1_1nuitrack_1_1Hand.html#ae005c3ecf4127c8c89da976bd23d0394) |
|          | The z coordinate of the hand in the world system.            |



### Disclaimer

This code is As-Is and was developed for a simple application test; there are a few items that could be optimized.