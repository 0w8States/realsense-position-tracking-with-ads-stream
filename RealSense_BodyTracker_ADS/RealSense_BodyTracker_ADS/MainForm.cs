﻿//-----------------------------------------------------------------------
// <copyright file="MainForm.cs" company="John Helfrich">
//      Copyright (c) John Helfrich. MIT License
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// </copyright>
// <author>John Helfrich</author>
//-----------------------------------------------------------------------
namespace RealSense_BodyTracker_ADS
{

    using System;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.Windows.Forms;
    using System.Runtime.InteropServices;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Xml;
    using TwinCAT.Ads;
    using System.IO;
    using TwinCAT;
    using nuitrack;
    using nuitrack.issues;

    public class MainForm : Form
    {

        // NuiTrack System Variables for Drawing
        private DirectBitmap _bitmap;
        private bool _visualizeColorImage = false;
        private bool _colorStreamEnabled = false;

        // NuiTrack System Variables for Sensor Modules
        private DepthSensor _depthSensor;
        private ColorSensor _colorSensor;
        private UserTracker _userTracker;
        private SkeletonTracker _skeletonTracker;
        private GestureRecognizer _gestureRecognizer;
        private HandTracker _handTracker;

        // NuiTrack System Variables for Data
        private DepthFrame _depthFrame;
        private SkeletonData _skeletonData;
        private HandTrackerData _handTrackerData;
        private IssuesData _issuesData = null;

        // Information about the PLC Struct on TwinCAT Side
        public struct PLCHandStruct
        {
            // The location of the valriable 
            public string POU;
            // Name of the variable inside the PLC
            public string VarName;

        }

        // ADS Connection Variables
        private TcAdsClient _tcClient = null;
        private AdsStream _adsStream = null;
        private BinaryReader _binRead = null;
        private int _notificationHandle = 0;
        private int[] _PLC_Handle_LeftHand = new int[7];
        private int[] _PLC_Handle_RightHand = new int[7];

        // Instances of the struct info on the PLC side
        private PLCHandStruct _PLCHandStruct_Right;
        private PLCHandStruct _PLCHandStruct_Left;

        // UI Variables for the form
        private ComboBox AdsCbBox;
        private Button AdsButtonConnect;
        private Button AdsButtonDisconnect;
        private Label AdsStatusLabel;
        private Label RouterStatusLabel;
        private Label AdsLocalCBLabel;
        private CheckBox AdsLocalCB;

        // XML Parser data for pulling ADS Static Routes
        private XmlNodeList RouteName;
        private XmlNodeList RouteAddress;
        private XmlNodeList RouteNetId;
        private const string StaticRoutePath = @"C:\TwinCAT\3.1\Target\StaticRoutes.xml";


        public MainForm()
        {
            AdsCbBox = new ComboBox();
            AdsButtonConnect = new Button();
            AdsButtonDisconnect = new Button();
            AdsStatusLabel = new Label();
            RouterStatusLabel = new Label();
            AdsLocalCBLabel = new Label();
            AdsLocalCB = new CheckBox();

            XML_ADS_Parser();

            if (AdsCbBox.Items.Count > 0)
            {
                AdsCbBox.SelectedIndex = 0;
            }

            this.Width = 800;
            this.Height = 800;


            AdsButtonConnect.Text = "ADS Connect";
            AdsButtonDisconnect.Text = "ADS Disconnect";
            AdsStatusLabel.Text = "Status: Disconnected";
            AdsLocalCBLabel.Text = "Local Target:";

            AdsButtonConnect.Width = AdsButtonDisconnect.Width = AdsStatusLabel.Width = AdsLocalCBLabel.Width = 100;
            AdsButtonConnect.Height = AdsButtonDisconnect.Height = AdsLocalCBLabel.Height = 50;
            AdsStatusLabel.Height = 15;
            AdsStatusLabel.Width = 200;

            AdsButtonConnect.Top = 500;
            AdsButtonDisconnect.Top = 500;
            AdsCbBox.Top = 529;
            AdsStatusLabel.Top = 501;
            AdsLocalCBLabel.Top = 535;
            AdsLocalCB.Top = 531;


            AdsButtonConnect.Left = 10;
            AdsButtonDisconnect.Left = 115;
            AdsCbBox.Left = 225;
            AdsStatusLabel.Left = 225;
            AdsLocalCBLabel.Left = 350;
            AdsLocalCB.Left = 425;

            AdsButtonConnect.Click += new EventHandler(this.AdsButtonConnect_Click);
            AdsButtonDisconnect.Click += new EventHandler(this.AdsButtonDisconnect_Click);
            AdsLocalCB.CheckedChanged += new EventHandler(this.TargetBox_Checked);

            this.Controls.Add(AdsLocalCB);
            this.Controls.Add(AdsLocalCBLabel);
            this.Controls.Add(AdsStatusLabel);
            this.Controls.Add(AdsCbBox);
            this.Controls.Add(AdsButtonConnect);
            this.Controls.Add(AdsButtonDisconnect);
            

            // Initialize Nuitrack. This should be called before using any Nuitrack module.
            // By passing the default arguments we specify that Nuitrack must determine
            // the location automatically.
            try
            {
                Nuitrack.Init("");
            }
            catch (nuitrack.Exception ex)
            {
                Task.Run(() =>
                {
                    MessageBox.Show(ex.Message);
                });
            }

            try
            {
                // Create and setup all required modules
                _depthSensor = DepthSensor.Create();
                _colorSensor = ColorSensor.Create();
                _userTracker = UserTracker.Create();
                _skeletonTracker = SkeletonTracker.Create();
                _handTracker = HandTracker.Create();
                _gestureRecognizer = GestureRecognizer.Create();
            }
            catch (nuitrack.Exception ex)
            {
                Task.Run(() =>
                {
                    MessageBox.Show(ex.Message);
                });
            }

            _depthSensor.SetMirror(false);

            // Add event handlers for all modules
            _depthSensor.OnUpdateEvent += onDepthSensorUpdate;
            _colorSensor.OnUpdateEvent += onColorSensorUpdate;
            _userTracker.OnUpdateEvent += onUserTrackerUpdate;
            _skeletonTracker.OnSkeletonUpdateEvent += onSkeletonUpdate;
            _handTracker.OnUpdateEvent += onHandTrackerUpdate;
            _gestureRecognizer.OnNewGesturesEvent += onNewGestures;

            // Add an event handler for the IssueUpdate event 
            Nuitrack.onIssueUpdateEvent += onIssueDataUpdate;

            // Create and configure the Bitmap object according to the depth sensor output mode
            OutputMode mode = _depthSensor.GetOutputMode();
            OutputMode colorMode = _colorSensor.GetOutputMode();

            if (mode.XRes < colorMode.XRes)
                mode.XRes = colorMode.XRes;
            if (mode.YRes < colorMode.YRes)
                mode.YRes = colorMode.YRes;

            _bitmap = new DirectBitmap(mode.XRes, mode.YRes);
            for (int y = 0; y < mode.YRes; ++y)
            {
                for (int x = 0; x < mode.XRes; ++x)
                    _bitmap.SetPixel(x, y, Color.FromKnownColor(KnownColor.Aqua));
            }

            // Set fixed form size
            this.MinimumSize = this.MaximumSize = new Size(mode.XRes, mode.YRes + 125);

            // Disable unnecessary caption bar buttons
            this.MinimizeBox = this.MaximizeBox = false;

            // Enable double buffering to prevent flicker
            this.DoubleBuffered = true;

            // Run Nuitrack. This starts sensor data processing.
            try
            {
                Nuitrack.Run();
            }
            catch (nuitrack.Exception ex)
            {
                Task.Run(() =>
                {
                    MessageBox.Show(ex.Message);
                });
            }

            this.Show();
            AdsLocalCB.Update();
            AdsLocalCBLabel.Update();
            AdsStatusLabel.Update();
            AdsCbBox.Update();
            AdsButtonConnect.Update();
            AdsButtonDisconnect.Update();
        }

        ~MainForm()
        {
            _bitmap.Dispose();
        }

        void AdsButtonConnect_Click(Object sender, EventArgs e)
        {
            try
            {
                _tcClient = new TcAdsClient();

                /* connect the client to the PLC */
                if (AdsLocalCB.Checked)
                    _tcClient.Connect(851);
                else
                    _tcClient.Connect(RouteNetId[AdsCbBox.SelectedIndex].InnerText, 851);

                _adsStream = new AdsStream(2);              /* stream storing the ADS state of the PLC */
                _binRead = new BinaryReader(_adsStream);    /* reader to read the state data */

                /* register callback to react on state changes of the local AMS router */
                _tcClient.AmsRouterNotification +=
                                        new AmsRouterNotificationEventHandler(AmsRouterNotificationCallback);


                _notificationHandle = _tcClient.AddDeviceNotification(
                                            (int)AdsReservedIndexGroups.DeviceData, /* index group of the device state*/
                                            (int)AdsReservedIndexOffsets.DeviceDataAdsState, /*index offsset of the device state */
                                            _adsStream, /* stream to store the state */
                                            AdsTransMode.OnChange,  /* transfer mode: transmit ste on change */
                                            0,  /* transmit changes immediately */
                                            0,
                                            null);

                /* register callback to react on state changes of the local PLC */
                _tcClient.AdsNotification += new AdsNotificationEventHandler(OnAdsNotification);

            }
            catch (AdsErrorException ex)
            {
                Task.Run(() =>
                {
                    MessageBox.Show(ex.Message);
                });
            }

            try
            {
                _PLCHandStruct_Left.POU = "MAIN";
                _PLCHandStruct_Left.VarName = "RealSense_LeftHand";

                _PLC_Handle_LeftHand[0] = _tcClient.CreateVariableHandle(_PLCHandStruct_Left.POU + "." + _PLCHandStruct_Left.VarName + "." + "X");
                _PLC_Handle_LeftHand[1] = _tcClient.CreateVariableHandle(_PLCHandStruct_Left.POU + "." + _PLCHandStruct_Left.VarName + "." + "Y");
                _PLC_Handle_LeftHand[2] = _tcClient.CreateVariableHandle(_PLCHandStruct_Left.POU + "." + _PLCHandStruct_Left.VarName + "." + "Click");
                _PLC_Handle_LeftHand[3] = _tcClient.CreateVariableHandle(_PLCHandStruct_Left.POU + "." + _PLCHandStruct_Left.VarName + "." + "Pressure");
                _PLC_Handle_LeftHand[4] = _tcClient.CreateVariableHandle(_PLCHandStruct_Left.POU + "." + _PLCHandStruct_Left.VarName + "." + "XReal");
                _PLC_Handle_LeftHand[5] = _tcClient.CreateVariableHandle(_PLCHandStruct_Left.POU + "." + _PLCHandStruct_Left.VarName + "." + "YReal");
                _PLC_Handle_LeftHand[6] = _tcClient.CreateVariableHandle(_PLCHandStruct_Left.POU + "." + _PLCHandStruct_Left.VarName + "." + "XReal");

                _PLCHandStruct_Right.POU = "MAIN";
                _PLCHandStruct_Right.VarName = "RealSense_RightHand";

                _PLC_Handle_RightHand[0] = _tcClient.CreateVariableHandle(_PLCHandStruct_Right.POU + "." + _PLCHandStruct_Right.VarName + "." + "X");
                _PLC_Handle_RightHand[1] = _tcClient.CreateVariableHandle(_PLCHandStruct_Right.POU + "." + _PLCHandStruct_Right.VarName + "." + "Y");
                _PLC_Handle_RightHand[2] = _tcClient.CreateVariableHandle(_PLCHandStruct_Right.POU + "." + _PLCHandStruct_Right.VarName + "." + "Click");
                _PLC_Handle_RightHand[3] = _tcClient.CreateVariableHandle(_PLCHandStruct_Right.POU + "." + _PLCHandStruct_Right.VarName + "." + "Pressure");
                _PLC_Handle_RightHand[4] = _tcClient.CreateVariableHandle(_PLCHandStruct_Right.POU + "." + _PLCHandStruct_Right.VarName + "." + "XReal");
                _PLC_Handle_RightHand[5] = _tcClient.CreateVariableHandle(_PLCHandStruct_Right.POU + "." + _PLCHandStruct_Right.VarName + "." + "YReal");
                _PLC_Handle_RightHand[6] = _tcClient.CreateVariableHandle(_PLCHandStruct_Right.POU + "." + _PLCHandStruct_Right.VarName + "." + "XReal");
            }
            catch (AdsErrorException ex)
            {
                Task.Run(() =>
                {
                    MessageBox.Show(ex.Message);
                });
            }

        }

        void AdsButtonDisconnect_Click(Object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < 7; i++)
                {
                    _tcClient.DeleteVariableHandle(_PLC_Handle_LeftHand[i]);
                    _tcClient.DeleteVariableHandle(_PLC_Handle_RightHand[i]);
                }
            }
            catch (AdsErrorException ex)
            {
                Task.Run(() =>
                {
                    MessageBox.Show(ex.Message);
                });
            }

            try
            {
                _tcClient.Dispose();
                AdsStatusLabel.Text = "Status: Disconnected";
            }
            catch (AdsErrorException ex)
            {
                Task.Run(() =>
                {
                    MessageBox.Show(ex.Message);
                });
            }
            AdsStatusLabel.Update();
        }


        void TargetBox_Checked(Object sender, EventArgs e)
        {
            if (AdsLocalCB.Checked)
                AdsCbBox.Visible = false;
            else
                AdsCbBox.Visible = true;

            AdsCbBox.Update();
        }



        /* callback function called on state changes of the PLC */
        void OnAdsNotification(object sender, AdsNotificationEventArgs e)
        {
            if (e.NotificationHandle == _notificationHandle)
            {
                AdsState plcState = (AdsState)_binRead.ReadInt16(); /* state was written to the stream */
                AdsStatusLabel.Invoke(new Action(() => AdsStatusLabel.Text = "Status: " + plcState.ToString()));
            }
            AdsStatusLabel.Invoke(new Action(() => AdsStatusLabel.Update()));
        }

        /* callback function called on state changes of the local AMS router */
        void AmsRouterNotificationCallback(object sender, AmsRouterNotificationEventArgs e)
        {
            RouterStatusLabel.Invoke(new Action(() => RouterStatusLabel.Text = e.State.ToString()));
            AdsStatusLabel.Invoke(new Action(() => AdsStatusLabel.Update()));
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                Nuitrack.onIssueUpdateEvent -= onIssueDataUpdate;

                _depthSensor.OnUpdateEvent -= onDepthSensorUpdate;
                _colorSensor.OnUpdateEvent -= onColorSensorUpdate;
                _userTracker.OnUpdateEvent -= onUserTrackerUpdate;
                _skeletonTracker.OnSkeletonUpdateEvent -= onSkeletonUpdate;
                _handTracker.OnUpdateEvent -= onHandTrackerUpdate;
                _gestureRecognizer.OnNewGesturesEvent -= onNewGestures;

                Nuitrack.Release();
                _tcClient.Dispose();
            }
            catch (AdsErrorException ex)
            {
                Task.Run(() =>
                {
                    MessageBox.Show(ex.Message);
                });
            }

            // Release Nuitrack and remove all modules
            try
            {
                Nuitrack.onIssueUpdateEvent -= onIssueDataUpdate;

                _depthSensor.OnUpdateEvent -= onDepthSensorUpdate;
                _colorSensor.OnUpdateEvent -= onColorSensorUpdate;
                _userTracker.OnUpdateEvent -= onUserTrackerUpdate;
                _skeletonTracker.OnSkeletonUpdateEvent -= onSkeletonUpdate;
                _handTracker.OnUpdateEvent -= onHandTrackerUpdate;
                _gestureRecognizer.OnNewGesturesEvent -= onNewGestures;

                Nuitrack.Release();
            }
            catch (nuitrack.Exception ex)
            {
                Task.Run(() =>
                {
                    MessageBox.Show(ex.Message);
                });
            }
        }


        private void XML_ADS_Parser()
        {
            XmlDocument xmlDoc = new XmlDocument(); // Create an XML document object
            xmlDoc.Load(StaticRoutePath); // Load the XML document from the specified file

            // Get elements
            RouteName = xmlDoc.GetElementsByTagName("Name");
            RouteAddress = xmlDoc.GetElementsByTagName("Address");
            RouteNetId = xmlDoc.GetElementsByTagName("NetId");

            // Load the ComboBox
            for (int i = 0; i < RouteName.Count; i++)
            {
                if (RouteName[i].InnerText.Length > 0)
                {
                    AdsCbBox.Items.Add(RouteName[i].InnerText);
                }
            }
        }

        // Switch visualization mode on a mouse click
        protected override void OnClick(EventArgs args)
        {
            base.OnClick(args);

            _visualizeColorImage = !_visualizeColorImage;
        }

        protected override void OnPaint(PaintEventArgs args)
        {
            base.OnPaint(args);

            // Update Nuitrack data. Data will be synchronized with skeleton time stamps.
            try
            {
                Nuitrack.Update(_skeletonTracker);
            }
            catch (LicenseNotAcquiredException ex)
            {
                Task.Run(() =>
                {
                    MessageBox.Show(ex.Message);
                });
            }
            catch (nuitrack.Exception ex)
            {
                Task.Run(() =>
                {
                    MessageBox.Show(ex.Message);
                });
            }

            // Draw a bitmap
            args.Graphics.DrawImage(_bitmap.Bitmap, new Point(0, 0));

            // Draw skeleton joints
            if (_skeletonData != null)
            {
                const int jointSize = 10;
                foreach (var skeleton in _skeletonData.Skeletons)
                {
                    SolidBrush brush = new SolidBrush(Color.FromArgb(255 - 40 * skeleton.ID, 0, 0));
                    foreach (var joint in skeleton.Joints)
                    {
                        args.Graphics.FillEllipse(brush, joint.Proj.X * _bitmap.Width - jointSize / 2,
                                                  joint.Proj.Y * _bitmap.Height - jointSize / 2, jointSize, jointSize);
                    }
                }
            }

            // Draw hand pointers
            if (_handTrackerData != null)
            {
                foreach (var userHands in _handTrackerData.UsersHands)
                {
                    if (userHands.LeftHand != null)
                    {
                        HandContent hand = userHands.LeftHand.Value;
                        int size = hand.Click ? 20 : 30;
                        Brush brush = new SolidBrush(Color.Aquamarine);
                        args.Graphics.FillEllipse(brush, hand.X * _bitmap.Width - size / 2, hand.Y * _bitmap.Height - size / 2, size, size);
                        ADS_StreamData(_PLC_Handle_LeftHand, hand);
                    }

                    if (userHands.RightHand != null)
                    {
                        HandContent hand = userHands.RightHand.Value;
                        int size = hand.Click ? 20 : 30;
                        Brush brush = new SolidBrush(Color.DarkBlue);
                        args.Graphics.FillEllipse(brush, hand.X * _bitmap.Width - size / 2, hand.Y * _bitmap.Height - size / 2, size, size);
                        ADS_StreamData(_PLC_Handle_RightHand, hand);
                    }
                }
            }

            // Update Form
            this.Invalidate();
        }

        private void ADS_StreamData(int[] varHandler, HandContent hand)
        {
            if (varHandler[0] != 0)
            {
                _tcClient.WriteAny(varHandler[0], hand.X);
                _tcClient.WriteAny(varHandler[1], hand.Y);
                _tcClient.WriteAny(varHandler[2], hand.Click);
                _tcClient.WriteAny(varHandler[3], hand.Pressure);
                _tcClient.WriteAny(varHandler[4], hand.XReal);
                _tcClient.WriteAny(varHandler[5], hand.YReal);
                _tcClient.WriteAny(varHandler[6], hand.ZReal);
            }
        }


        private void onIssueDataUpdate(IssuesData issuesData)
        {
            _issuesData = issuesData;
        }

        // Event handler for the DepthSensorUpdate event
        private void onDepthSensorUpdate(DepthFrame depthFrame)
        {
            _depthFrame = depthFrame;
        }

        // Event handler for the ColorSensorUpdate event
        private void onColorSensorUpdate(ColorFrame colorFrame)
        {
            if (!_visualizeColorImage)
                return;

            _colorStreamEnabled = true;

            float wStep = (float)_bitmap.Width / colorFrame.Cols;
            float hStep = (float)_bitmap.Height / colorFrame.Rows;

            float nextVerticalBorder = hStep;

            Byte[] data = colorFrame.Data;
            int colorPtr = 0;
            int bitmapPtr = 0;
            const int elemSizeInBytes = 3;

            for (int i = 0; i < _bitmap.Height; ++i)
            {
                if (i == (int)nextVerticalBorder)
                {
                    colorPtr += colorFrame.Cols * elemSizeInBytes;
                    nextVerticalBorder += hStep;
                }

                int offset = 0;
                int argb = data[colorPtr]
                    | (data[colorPtr + 1] << 8)
                    | (data[colorPtr + 2] << 16)
                    | (0xFF << 24);
                float nextHorizontalBorder = wStep;
                for (int j = 0; j < _bitmap.Width; ++j)
                {
                    if (j == (int)nextHorizontalBorder)
                    {
                        offset += elemSizeInBytes;
                        argb = data[colorPtr + offset]
                            | (data[colorPtr + offset + 1] << 8)
                            | (data[colorPtr + offset + 2] << 16)
                            | (0xFF << 24);
                        nextHorizontalBorder += wStep;
                    }

                    _bitmap.Bits[bitmapPtr++] = argb;
                }
            }
        }

        // Event handler for the UserTrackerUpdate event
        private void onUserTrackerUpdate(UserFrame userFrame)
        {
            if (_visualizeColorImage && _colorStreamEnabled)
                return;
            if (_depthFrame == null)
                return;

            const int MAX_LABELS = 7;
            bool[] labelIssueState = new bool[MAX_LABELS];
            for (UInt16 label = 0; label < MAX_LABELS; ++label)
            {
                labelIssueState[label] = false;
                if (_issuesData != null)
                {
                    FrameBorderIssue frameBorderIssue = _issuesData.GetUserIssue<FrameBorderIssue>(label);
                    labelIssueState[label] = (frameBorderIssue != null);
                }
            }

            float wStep = (float)_bitmap.Width / _depthFrame.Cols;
            float hStep = (float)_bitmap.Height / _depthFrame.Rows;

            float nextVerticalBorder = hStep;

            Byte[] dataDepth = _depthFrame.Data;
            Byte[] dataUser = userFrame.Data;
            int dataPtr = 0;
            int bitmapPtr = 0;
            const int elemSizeInBytes = 2;
            for (int i = 0; i < _bitmap.Height; ++i)
            {
                if (i == (int)nextVerticalBorder)
                {
                    dataPtr += _depthFrame.Cols * elemSizeInBytes;
                    nextVerticalBorder += hStep;
                }

                int offset = 0;
                int argb = 0;
                int label = dataUser[dataPtr] | dataUser[dataPtr + 1] << 8;
                int depth = Math.Min(255, (dataDepth[dataPtr] | dataDepth[dataPtr + 1] << 8) / 32);
                float nextHorizontalBorder = wStep;
                for (int j = 0; j < _bitmap.Width; ++j)
                {
                    if (j == (int)nextHorizontalBorder)
                    {
                        offset += elemSizeInBytes;
                        label = dataUser[dataPtr + offset] | dataUser[dataPtr + offset + 1] << 8;
                        if (label == 0)
                            depth = Math.Min(255, (dataDepth[dataPtr + offset] | dataDepth[dataPtr + offset + 1] << 8) / 32);
                        nextHorizontalBorder += wStep;
                    }

                    if (label > 0)
                    {
                        int user = label * 40;
                        if (!labelIssueState[label])
                            user += 40;
                        argb = 0 | (user << 8) | (0 << 16) | (0xFF << 24);
                    }
                    else
                    {
                        argb = depth | (depth << 8) | (depth << 16) | (0xFF << 24);
                    }

                    _bitmap.Bits[bitmapPtr++] = argb;
                }
            }
        }

        // Event handler for the SkeletonUpdate event
        private void onSkeletonUpdate(SkeletonData skeletonData)
        {
            _skeletonData = skeletonData;
        }

        // Event handler for the HandTrackerUpdate event
        private void onHandTrackerUpdate(HandTrackerData handTrackerData)
        {
            _handTrackerData = handTrackerData;
        }

        // Event handler for the gesture detection event
        private void onNewGestures(GestureData gestureData)
        {
            // Display the information about detected gestures in the console
            foreach (var gesture in gestureData.Gestures)
                Console.WriteLine("Recognized {0} from user {1}", gesture.Type.ToString(), gesture.UserID);
        }
    }
}
